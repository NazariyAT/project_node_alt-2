const User = require("../models/user.model");
const HTTPSTATUSCODE = require("../../utils/httpStatusCode");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const passport = require("passport");

const registerUser =  (req,res,next) => {
    try{
        passport.authenticate('register', (err,user)=>{
            if (err) {
                return next(err)
            }
            req.logIn(user, (err)=>{
                if(err){
                    return next(err)
                }
                return res.status(201).json(user)
            });
        })(req);
    }catch(err){
        return next(err)
    }
}

const logInUser =  (req,res,next) => {
    try{
        passport.authenticate('login', (err, user) => {
            if (err) {
                return next(err)
            }
            req.logIn(user, (err) => {
                if (err) {
                    return next(err);
                }
                return res.status(200).json(user)
            });
        })(req);
    }catch(err){
        return next(err)
    } 
}

const logOutUser =  (req,res,next) => {
    try{
        if (req.user) {
        req.logout()
        req.session.destroy(()=>{
            res.clearCookie('connect.sid');
            return res.status(200).json('See you soon!');
        });
    }else{
        return res.sendStatus(304)
    }

    }catch(err){
        return next(err)
    }
}

module.exports={
    registerUser,
    logInUser,
    logOutUser
}