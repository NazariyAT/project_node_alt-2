const express = require("express");
const { registerUser, logInUser, logOutUser} = require("../controllers/user.controllers");
const router = express.Router();

router.post("/register", registerUser);
router.post("/login", logInUser);
router.post("/logout", logOutUser);


module.exports = router;