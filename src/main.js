
const express = require("express");
const logger = require("morgan");
const passport = require("passport");
const session = require('express-session')
//DB
const { connect } = require("./config/database")
require('./authentication/passport')
const HTTPSTATUSCODE = require("./utils/httpStatusCode");
const UserRoutes = require("./api/routes/user.routes");

connect()
const app = express();
const cors = require("cors");
const MongoStore = require("connect-mongo");

app.use(
  session({
    secret: 'secret_node',
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 3600000
    },
    store: MongoStore.create({
      mongoUrl: process.env.MONGO_URI,
    })
  })
);
app.use(passport.initialize())
app.use(passport.session());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.use(cors({
  origin: ['http://localhost:3000', 'http://localhost:4200'],
  credentials: true,
}));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(logger("dev"));

//ROUTES

app.use('/user', UserRoutes)

//ERRORS
app.use((req, res, next) => {
  let err = new Error();
  err.status = 404;
  err.message = HTTPSTATUSCODE[404];
  next(err);
});

app.use((err, req, res, next) => {
  return res.status(err.status || 500).json(err.message || 'Unexpected ERROR');
})

//SERVER
app.listen(3000, () => {
  console.log("Node server listening on port : 3000");
})